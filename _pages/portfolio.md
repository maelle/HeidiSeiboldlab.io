---
permalink: /portfolio/
title: "Workshop Portfolio"
header:
  overlay_image: "../assets/images/Header-Image.png"
author_profile: false
---



- [Reproducible Data Science Crash Course](rds_crashcourse)
- [Open Science Crash Course](os_crashcourse)
- [Open Science Hero Workshop](os_hero)
- [Reproducible Data Science Summer School](rds_school)
- ... 

Ask me about further workshop ideas that fit your needs!



All workshops and trainings are available in German and English language. 

[Get in touch!](/impressum/){: .btn .btn--success}


