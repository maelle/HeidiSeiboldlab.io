---
title: Contact
permalink: /contact/
aside: true
---

The best way to reach me is to send an [email](mailto:heidi@heidiseibold.de)
or chat with me on 
<a rel="me" href="https://fosstodon.org/@HeidiSeibold">Mastodon</a>.

<p align="center">
<script async data-uid="17875ed937" src="https://heidiseibold.ck.page/17875ed937/index.js"></script>
</p>

### Address:
Heidi Seibold  
c/o MUCBOOK Clubhouse  
Elsenheimerstr. 48  
80687 München, Germany   

