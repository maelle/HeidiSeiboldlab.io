---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#6a0498"
#  ioverlay_image: "/assets/images/header_stars.jpg"
excerpt: "Open and Reproducible Data Science"
feature_row:
  - image_path: /assets/images/workshop.jpg
    alt: "."
    title: "Workshops + Training"
    excerpt: "I offer workshops and trainings on Reproducible Data Science and on Open Science."
    url: "/portfolio/"
    btn_class: "btn--primary"
    btn_label: "View portfolio"
  - image_path: /assets/images/rope.jpg
    alt: "."
    title: "Consulting"
    excerpt: "Let me help you get the best out of your projects. May it be about data
science, open science or community management."
  - image_path: /assets/images/conference.jpg
    alt: "."
    title: "Events"
    excerpt: "Book me as a speaker in live or online events. I also organize and moderate events. Currently I am organising an Open Science Retreat."
    url: "https://open-science-retreat.gitlab.io/"
    btn_class: "btn--primary"
    btn_label: "Open Science Retreat" 
#  - image_path: /assets/images/W-media.png
#    alt: "."
#    title: "Media"
#    excerpt: "I produce audio and video material."
#    url: "/podcasts/"
#    btn_class: "btn--primary"
#    btn_label: "Check out my podcasts"
---

<!-- ![](/assets/images/Heidi_round.png){: width="250" .align-center} -->

# Services:

{% include feature_row %}



All services are available in German and English language. 
{: .text-center}

[View Testimonials](/testimonials/){: .btn .btn--success .btn--large}
{: .text-center}

---

<p align="center">
<script async data-uid="17875ed937" src="https://heidiseibold.ck.page/17875ed937/index.js"></script>
</p>
