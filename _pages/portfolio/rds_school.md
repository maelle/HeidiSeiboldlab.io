---
permalink: /portfolio/rds_school/
---

## Reproducible Data Science Summer School
---

|**Format:**        |  In person, 4.5-5 day summer/winter school (optimally in a nice location), 2-3 exciting keynotes + workshop sessions + social/networking events|
|**Size:**          | 5 - 35 participants |
|**Prerequisites:** | Basic coding skills in e.g. R, Python or similar, basic knowledge of data analysis |

---

This summer/winter school will turn you into a science champion. In the course
of this week you will learn everything that makes a great and reproducible
research project and you will leave with the most important steps already
implemented for your current project. In 3 inspiring keynotes science champions
will share their experiences and wisdom with us.  We have some great social
events planned for you so that this school will serve as the most fun and
fruitful event of your year.


#### Topics:

- What is reproducible research?
- Project organisation for reproducible research
- Reproducible analyses 
- Publication of research output
- Science communication
- Topics from the keynotes (dependend on speakers)

