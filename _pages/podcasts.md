---
permalink: /podcasts/
title: "Podcasts"
gallery:
  - image_path: /assets/images/Podcast_RebootAcademia.png
    alt: "reboot academia - Forschung und Lehre neu gedacht"
    url: "https://anchor.fm/reboot-academia"
  - image_path: /assets/images/Podcast_OpenScienceStories.png
    alt: "Open Science Stories"
    url: "https://anchor.fm/opensciencestories"
---


{% include gallery layout="half" %}

