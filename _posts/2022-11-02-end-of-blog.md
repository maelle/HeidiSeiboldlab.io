---
layout: posts
title:  "End of this blog"
date:   2022-11-02
---

Since I have a newsletter now, I will let this blog sleep. 

<iframe src="https://heidiseibold.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe>

Please subscribe to the newsletter to get updates and all kinds of free wisdom and infos.
