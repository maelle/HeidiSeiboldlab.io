---
layout: posts
title:  "Newsletter"
date:   2022-10-09
categories:
 - reproducible research
 - tips
---

I am finally starting a newsletter! :tada:

The topic: **Open and Reproducible Data Science**.

<iframe src="https://heidiseibold.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe>

Why should you sign up? I have gathered knowledge on Open Science and
reproducible data science and I want to share it with you. To help you become
the best researcher you can be.

Whether you are a psychologist, medical researcher, historian, biologist, ...
If you work with data, this newsletter is for you!

The first few newsletters will be sort of an e-mail course:
First steps towards Reproducible Research.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The first few newsletters will be sort of an e-mail course:<br><br>▶️First steps towards reproducible research 👣<br><br>The image gives an overview on what we&#39;ll cover. <a href="https://t.co/ErdbkqCZrG">pic.twitter.com/ErdbkqCZrG</a></p>&mdash; Heidi Seibold (@HeidiBaya) <a href="https://twitter.com/HeidiBaya/status/1578731221466877953?ref_src=twsrc%5Etfw">October 8, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

