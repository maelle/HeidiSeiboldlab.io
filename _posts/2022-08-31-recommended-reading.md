---
layout: posts
title:  "Recommended reading"
date:   2022-08-31
categories:
 - reproducible research
 - tips
feature_image: "https://picsum.photos/id/1073/1200/"
---

I don't invent the things I teach in my workshops. They're well established
and often well researched tools and ideas. So here is a list of resources I 
use for the workshops and as such I would of course recommend them for you 
to read :wink:

- [The Turing Way](https://the-turing-way.netlify.app) is for sure the book
  that resembles the topics I teach best. It is a book (and a community) about
reproducible, ethical and collaborative data science. 
- [The Open Science training
  handbook](https://www.fosteropenscience.eu/content/open-science-training-handbook)
is a nice resource for trainers and I use it a lot for the great CC-0 images.
- [Twitter](https://twitter.com) is a fun and often surprisingly helpful
  resource to me (in particular check out the hashtag #OpenScience). You might
say, that recommending to read Twitter is stupid, but hey, I am not one for
reading a lot and that is what I like to read :bird:. On my [own
account](https://twitter.com/HeidiBaya) I ofthen share thoughts, ressources
and updates as well. 


